# DASyR - Document Analysis System for Systematic Reviews

Initialize this project on a collection of pdf documents. Then use the term recommendation system.

## Setup

### Prerequirements
 * Solr (tested with version 8.5.2)
 * some DBMS + python client(tested with sqlite and mysql): system is DBMS agnostic since it uses SQLAlchemy
 * GROBID: only for initialization phase 

### Installation
 1. clone this repository
 2. pip install everything in /backend
 3. npm install everything in /frontend
 4. create a core in solr
 5. create a database in your DBMS
 6. **update config file, so that everything is pointed to correctly**
 7. execute backend/db/setup_db.py
 8. execute backend/db/insert_user.py
 9. put your source pdf files in backend/data/source (nested folders are fine)

## Initialization
 	1. run solr server
	2. run grobid server
	3. run backend/initialization/pipeline.py
	4. stop grobid server
	5. stop solr server
	6. optional: delete contents of artifacts/txt artifacts/tei, those are only byproducts of the initialization process
 
## Annotation
	* run frontend (ng serve)
	* run solr server (bin/solr start)
	* run backend (flask)

