import os

from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from config import config
from controllers.annotation import annotation
from controllers.auth import auth
from db.database import db

app = Flask(__name__)
app.register_blueprint(auth)
app.register_blueprint(annotation)
cors = CORS(app, resources={r'.*': {'origins': config['frontend_base_uri']}})

app.config['JWT_SECRET_KEY'] = os.environ.get('JWT_SECRET_KEY', 'daysr_jwt_secret_key')
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 14400  # 4 hours in seconds
jwt = JWTManager(app)

# app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = config['db_uri']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)
if __name__ == '__main__':
    app.run()
