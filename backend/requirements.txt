nltk==3.5
Flask==1.1.2
passlib==1.7.2
pysolr==3.9.0
gensim==3.8.3

setuptools~=50.3.0
requests~=2.24.0
mysql-connector-python~=8.0.22
SQLAlchemy~=1.3.19
flask-cors
flask-jwt-extended
