config = {
    #'frontend_base_uri': 'http://localhost:4200',
    'frontend_base_uri': '*',
    'solr_base_uri': 'http://localhost:8983/solr/dasyr',
    'w2v_file_name': 'w2v-corpus-trained',
    'db_uri': 'sqlite:///db/dasyr.db'
}
initialization_config = {
    'phrasing_passes': 2,
    'stop_words_language': 'english',
}
