import os

from gensim.models.phrases import Phraser

from initialization.embedding import RISIterator, line_split_concat

if __name__ == '__main__':

    cur_dir = os.path.dirname(__file__)
    txt_dir = os.path.join(cur_dir, '../data/ris')
    txt_paths = [os.path.join(txt_dir, 'ris_out.txt')]
    phr1 = Phraser.load(str(os.path.join(cur_dir, '../data/artifacts/embedding/ris_phrases_model1')))
    phr2 = Phraser.load(str(os.path.join(cur_dir, '../data/artifacts/embedding/ris_phrases_model2')))
    for line in RISIterator(txt_paths, line_split_concat):
        print(line)
        print(phr2[phr1[line]])
