import pysolr
from config import config

if __name__ == '__main__':
    solr = pysolr.Solr(config['solr_base_uri'])
    hl_dict = {
        'hl': 'true',
        'hl.preserveMulti': 'true',
        'hl.fragsize': 0,
        'hl.fl': 'content',
        'hl.simple.pre': '<>',
        'hl.simple.post': '<>',
        'hl.requireFieldMatch': 'true',
        'hl.usePhraseHighlighter': 'true',
    }
    res = solr.search('content:"mean average precision"', **hl_dict)
    print(res.highlighting)

    for el in res:
        print(res.highlighting[el['id']]['content'])
        print(el['content'])
        print([x.replace('<>', '') for x in res.highlighting[el['id']]['content']] == el['content'])
