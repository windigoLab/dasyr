import json
import os
import pandas as pd
import statistics

if __name__ == '__main__':
    cur_dir = os.path.dirname(__file__)
    with open(os.path.join(cur_dir, 'candidates_evaluation2.json'), 'r') as fp:
        data = json.load(fp)
        df = pd.DataFrame(data['results'])
        original_len = len(data['original_terms'])

        original_len_not_none = len(data['original_terms_not_none'])
        print(original_len)
        print(original_len_not_none)
        df['found_len'] = df.apply(lambda row: original_len - len(row['not_found_terms']), axis=1)
        df['not_none_found_len'] = df.apply(lambda row: original_len_not_none - len(row['not_none_not_found_terms']),
                                            axis=1)
        df['found_len_percent'] = df.apply(lambda row: (row['found_len'] / original_len) * 100, axis=1)
        df['not_none_found_len_percent'] = df.apply(
            lambda row: (row['not_none_found_len'] / original_len_not_none) * 100, axis=1)

        df = df.drop(['not_found_terms', 'not_none_not_found_terms', 'seeds'], axis=1)
        pd.set_option('display.max_columns', 100)
        df_grouped = df.groupby(['sample_size', 'neighbour_size']).mean()
        print(df_grouped.unstack('neighbour_size', fill_value=0).drop('repetition', axis=1).round(1))
        # print(pd.crosstab(df_grouped['sample_size'], df_grouped['neighbour_size']))
        print(data.keys())
        ks = [5, 10, 15, 20, 15]
        ns = [2, 4, 8, 16, 32]

        results = [[d for d in data['results'] if d['neighbour_size'] == k and d['sample_size'] == n] for k in ks for n
                   in ns]

        averaged_results = []
        for res in results:
            print(res)
            # print(res['neighbour_size'], res['sample_size'], res['repetition'], len(res['not_found_terms']),
            #       len(res['not_none_not_found_terms']), res['mean'], res['median'], res['max'])
