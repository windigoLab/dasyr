import json
import os

from service.term_recommendation_engine import TermRecommendationEngine

if __name__ == '__main__':

    cur_dir = os.path.dirname(__file__)
    with open(os.path.join(cur_dir, 'candidates_evaluation2.json'), 'r') as fp:
        data = json.load(fp)
        original_terms = data['original_terms_not_none']
        emb = TermRecommendationEngine()
        print(emb.get_n_most_similar("r-prec", 4))
        if False:
            amount = 0
            for t in original_terms:
                res = emb.get_n_most_similar(t, 1)
                if len(res) == 0:
                    amount += 1
            print(amount)
