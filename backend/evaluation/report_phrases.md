The difference between 2 or more iterations seems minimal.
I trained phrases 3 times, first with the plain words as input, and in the subsequent iterations with the output of the 
previous phrase-model. In the subsequent runs I halved the threshold and min occurrence, as per advice from Word2Vec paper.
The third run should only make a difference in terms of concatenating multiple phrases, or another word to a phrase.
When testing whether applying phrase[phrase[sentence]] or phrase[phrase[phrase[sentence]]],
it only made a difference in a handful of sentences, and in those cases it was not producing valuable phrase concatenations:

I additionally found that in the dataset often terms appear that seem were originally meant to be one, but were split with
a hyphen due to being close to the line end. For example finding adjacent elements such as 'semi-', 'automatically', or 
're-' 'sults'. I decided to concatenate them using the hyphen, as I found that to be the better solution more often.
Another idea to be tried would be to use the occurrence of the concatenated term in a dictionary, and decide upon that how to concat the terms.

The finding of phrases works surprisingly well. Setting out for this task, I was given two examples of what kind of phrases
would be nice to identify: 'reciprocal rank' and 'mean average precision', both were identified.

found that
def phrases_test_set():
    return {'reciprocal_rank',
            'average_precision',
            'mean_average_precision',
            'geometric_mean_average_precision',
            'inverse_document_frequency',
            'cumulative_gain',
            'correlation_coefficient',
            'term_frequency',
            'word_sense_disambiguation'
            }

store individual iterations' phraser model, and apply it in original order.
2 iterations produces trigrams and quadgrams, where already most quadgrams by my estimation are irrelevant.
So further iterations are not productive.

Code:
txt_dir = os.path.join(cur_dir, '../data/artifacts/txt/')
    txt_paths = [str(path.absolute()) for path in Path(txt_dir).rglob(f'*.txt')]
    # phr = build_phrases(txt_paths)
    # phr.save(str(os.path.join(cur_dir, 'phrases_model.txt')))
    phr = Phrases.load(str(os.path.join(cur_dir, 'phrases_model.txt')))
    for el in W2VIterator(txt_paths):
        hyp1 = phr[phr[phr[el]]] == phr[phr[phr[phr[el]]]]
        hyp2 = phr[phr[el]] == phr[phr[phr[el]]]
        # hyp3 = phr[el] == phr[phr[phr[el]]]
        if not hyp1 or not hyp2:
            set1 = set(phr[el])
            set2 = set(phr[phr[el]])
            set3 = set(phr[phr[phr[el]]])
            print('set1 vs set2', set1.symmetric_difference(set2))
            print('set2 vs set3', set2.symmetric_difference(set3))
            print(hyp1, hyp2)
            print([t for t in phr[el] if '_' in t])

Output:
set1 vs set2 {'using_trec_eval', 'trec_eval', 'using'}
set2 vs set3 {'using_trec_eval', 'using_trec_eval_program', 'program'}
True False
['proposed_hiemstra', 'retrieval_performance', 'map_values', 'trec_eval']
set1 vs set2 {'using_trec_eval', 'trec_eval', 'using'}
set2 vs set3 {'using_trec_eval', 'using_trec_eval_program', 'program'}
True False
['english_language', 'retrieval_performance', 'map_values', 'trec_eval']
set1 vs set2 {'using_trec_eval', 'using', 'trec_eval'}
set2 vs set3 {'using_trec_eval', 'using_trec_eval_program', 'program'}
True False
['trec_eval']
set1 vs set2 {'using_trec_eval', 'trec_eval', 'using'}
set2 vs set3 {'using_trec_eval', 'using_trec_eval_program', 'program'}
True False
['trec_eval']
set1 vs set2 {'using_trec_eval', 'trec_eval', 'using'}
set2 vs set3 {'using_trec_eval', 'program', 'using_trec_eval_program'}
True False
['lemur_project', 'search_engine', 'presented_section', 'training_set', 'web_track', 'trec_eval']
set1 vs set2 {'using_trec_eval', 'trec_eval', 'using'}
set2 vs set3 {'using_trec_eval', 'using_trec_eval_program', 'program'}
True False
['trec_eval']