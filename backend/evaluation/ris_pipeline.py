import json
import os
from pathlib import Path

from gensim.models.phrases import Phraser

import initialization.extraction as extraction
import pysolr
from nltk import sent_tokenize

from config import config
from initialization.embedding import train_w2v_model, build_phrases, RISIterator, line_split_concat


def get_all_paths(base_dir, extension):
    return [str(path.absolute()) for path in Path(base_dir).rglob(f'*.{extension}')]


def get_solr_formatted_data(source_json):
    doc_id = source_json['id']
    sentences = sent_tokenize(' '.join(source_json['content']))
    clumps = [sentences[i * 5:(i + 1) * 5] for i in range(len(sentences) // 5)]
    clumps.append(sentences[(len(sentences) // 5) * 5:len(sentences)])
    return [{'id': doc_id + '/' + str(i), 'content': c, 'sentence_count': len(c), 'fragment_nr': i} for i, c in
            enumerate(clumps) if len(c) > 0]


def setup_solr_index():
    print("Setting up Solr index:")
    for i, p in enumerate(txt_paths):
        with open(p, 'r') as in_f:
            for line in in_f:
                data = json.loads(line)
                solr_data = get_solr_formatted_data(data)
                solr.add(solr_data)


if __name__ == '__main__':
    cur_dir = os.path.dirname(__file__)
    txt_dir = os.path.join(cur_dir, '../data/ris')

    # config_path = os.path.join(cur_dir, 'grobid/config.json')

    # solr = pysolr.Solr('http://localhost:8983/solr/ris', always_commit=True)
    # extraction.grobid_extract(config_path, in_dir, tei_dir)
    # tei_paths = get_all_paths(tei_dir, 'xml')
    # print(len(tei_paths))
    # extraction.tei_to_filtered_txt(tei_paths, 'txt')
    # tei_paths = get_all_paths(tei_dir, 'xml')
    txt_paths = [os.path.join(txt_dir, 'ris_out.txt')]
    # setup_solr_index()
    # build_phrases(txt_paths, 2, RISIterator(txt_paths, line_split_concat))
    phr1 = Phraser.load(str(os.path.join(cur_dir, '../data/artifacts/embedding/ris_phrases_model1')))
    phr2 = Phraser.load(str(os.path.join(cur_dir, '../data/artifacts/embedding/ris_phrases_model2')))
    train_w2v_model(txt_paths, [phr1, phr2], RISIterator(txt_paths, line_split_concat))
