import json
import os
import re
import statistics
from collections import defaultdict

from service.term_recommendation_engine import TermRecommendationEngine
from controllers.annotation import format_word
import random
import mysql.connector

REPETITIONS = 3
SAMPLE_SIZES = [2, 4, 8, 16, 32]
NEIGHBOUR_SIZES = [5, 10, 15, 20, 25]


def sample_dataset(rows, categories, n):
    sample = []
    for category in categories:
        category_terms = [row[0] for row in rows if row[1] == category]
        print(category)
        sample += [transform_bb_symbols(format_word(el)) for el in random.sample(category_terms, n)]
    return sample


def get_iteration_statistics(term_iterations):
    values = term_iterations.values()
    mean = statistics.mean(values)
    mx = max(values)
    median = statistics.median(values)
    return mean, median, mx


def find_all_terms(engine, seeds, reference_terms, neighbours):
    terms = set(seeds)
    current_terms = set(seeds)
    steps = 0
    term_iterations = {}
    while len(current_terms) > 0:
        print(len(current_terms))
        term = format_word(current_terms.pop())
        most_sim_terms = [res[0] for res in engine.get_n_most_similar(term, neighbours)]

        new_terms = {t for t in most_sim_terms if t in reference_terms and t not in terms}
        for new_term in new_terms:
            term_iterations[new_term] = term_iterations.get(term, 0) + 1
        current_terms |= new_terms
        terms |= new_terms
    mean, median, mx = get_iteration_statistics(term_iterations)
    return terms, mean, median, mx


def transform_bb_symbols(term):
    bb_map = {
        'zero': 0,
        'one': 1,
        'two': 2,
        'three': 3,
        'four': 4,
        'five': 5,
        'six': 6,
        'seven': 7,
        'eight': 8,
        'nine': 9,
        'at': '@',
        'hyphen': '-'
    }
    for key, val in bb_map.items():
        term = term.replace('bb' + key + 'bb', str(val))
    return term
    # parts = term.split('bb')
    # out_parts = []
    # for part in parts:
    #     if part in bb_map:
    #         out_parts.append(str(bb_map[part]))
    #         print(out_parts)
    #         print(parts)
    #     else:
    #         out_parts.append(part)
    # return ''.join(out_parts)


def contains_bb_symbol(term):
    bb_numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    bb_symbols = ['at', 'hyphen']
    for j in bb_numbers + bb_symbols:
        if 'bb' + j + 'bb' in term:
            return True
    return False


if __name__ == '__main__':
    engine = TermRecommendationEngine()
    conn = mysql.connector.connect(
        host='localhost',
        user='evaluation',
        password='',
        database='admire_full'
    )
    query_terms = "SELECT DISTINCT idSeed, idCategory FROM annotation WHERE idCategory NOT IN ('NULL', 'Garbage')"
    query_terms_not_none = "SELECT DISTINCT idSeed, idCategory FROM annotation WHERE idCategory NOT IN ('NULL', 'Garbage', 'None')"
    query_categories = "SELECT id FROM category WHERE id NOT IN ('Skip', 'Garbage')"
    cur = conn.cursor()
    cur.execute(query_terms)
    original_rows = cur.fetchall()
    cur.execute(query_categories)
    original_categories = [c[0] for c in cur.fetchall()]
    cur.execute(query_terms_not_none)
    original_not_none_rows = cur.fetchall()
    print(original_categories)
    print(len(original_rows))
    print(len(original_not_none_rows))
    print(original_rows[:2])
    original_term_set = {transform_bb_symbols(format_word(row[0])) for row in original_rows}
    original_term_set_not_none = {transform_bb_symbols(format_word(row[0])) for row in original_not_none_rows}
    print([el for el in original_term_set if contains_bb_symbol(el)])
    print(len(original_term_set))
    evaluation_results = []
    for neighbour_size in NEIGHBOUR_SIZES:
        for sample_size in SAMPLE_SIZES:
            for i in range(REPETITIONS):
                current_eval = {'neighbour_size': neighbour_size, 'sample_size': sample_size, 'repetition': i,
                                'seeds': sample_dataset(original_rows, original_categories, sample_size)}
                print(current_eval)
                terms_found, mean, median, mx = find_all_terms(engine, current_eval['seeds'], original_term_set,
                                                               neighbour_size)
                print(len(terms_found))
                print(mean, median, mx)
                not_found_terms = [t for t in original_term_set.difference(terms_found)]
                not_found_terms_not_none = [t for t in original_term_set_not_none.difference(terms_found)]
                print(len(not_found_terms))
                print(len(not_found_terms_not_none))
                current_eval['median'] = median
                current_eval['mean'] = mean
                current_eval['max'] = mx
                current_eval['not_found_terms'] = not_found_terms
                current_eval['not_none_not_found_terms'] = list(not_found_terms_not_none)
                evaluation_results.append(current_eval)
    evaluation = {
        'categories': original_categories,
        'original_terms': list(original_term_set),
        'original_terms_not_none': list(original_term_set_not_none),
        'results': evaluation_results
    }

    cur_dir = os.path.dirname(__file__)
    with open(os.path.join(cur_dir, 'candidates_evaluation2.json'), 'w+') as fp:
        json.dump(evaluation, fp)
