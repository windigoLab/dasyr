from app import app
from dao.annotation import Annotation
from dao.annotation_class import AnnotationClass
from dao.skipped_fragment import SkippedFragment
from dao.term import Term
from db.database import db

if __name__ == '__main__':
    with app.app_context():
        amount = SkippedFragment.query.delete()
        print(f'{amount} skipped fragments deleted')

        amount = Annotation.query.delete()
        print(f'{amount} annotations deleted')
        terms = Term.query.all()
        for term in terms:
            term.parent_id = None
        db.session.commit()
        amount = Term.query.delete()
        print(f'{amount} terms deleted')
        amount = AnnotationClass.query.delete()
        print(f'{amount} annotation classes deleted')
        db.session.commit()
