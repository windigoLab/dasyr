from app import app
from db.database import db
from dao.annotation import Annotation
from dao.annotation_class import AnnotationClass
from dao.term import Term
from dao.user import User
from dao.skipped_fragment import SkippedFragment

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    print('db and tables created')
