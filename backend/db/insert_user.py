import argparse

from passlib.hash import sha256_crypt

from app import app
from dao.user import User
from db.database import db

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    args = {}
    args['username'] = input('username: ')
    args['password'] = sha256_crypt.hash(input('password: '))
    user = User(username=args['username'], password=args['password'])
    with app.app_context():
        db.session.add(user)
        db.session.commit()
    print('User "%r" added successfully' % args['username'])
