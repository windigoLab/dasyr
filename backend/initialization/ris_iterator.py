import json

from nltk import sent_tokenize, word_tokenize


class RISIterator:
    def __init__(self, paths, format_word, word_ok, transformation_function=lambda x: x):
        self.paths = paths
        self.transformation_function = transformation_function
        self.format_word = format_word
        self.word_ok = word_ok

    def __iter__(self):
        for p in self.paths:
            with open(p, 'r') as in_f:
                for line in in_f:
                    content = ' '.join(json.loads(line)['content'])
                    for sent in sent_tokenize(content):
                        yield self.transformation_function(
                            [self.format_word(w) for w in word_tokenize(sent) if self.word_ok(w)])
