import os
from pathlib import Path

from gensim.models.phrases import Phraser

import initialization.extraction as extraction
import pysolr
from nltk import sent_tokenize

from config import config, initialization_config
from initialization.embedding import train_w2v_model, build_phrases, format_word, word_ok, line_split_concat
from initialization.w2v_sentence_iterator import SentenceIterator


def get_all_paths(base_dir, extension):
    return [str(path.absolute()) for path in Path(base_dir).rglob(f'*.{extension}')]


def get_solr_formatted_data(file_path, source_text):
    doc_id = file_path.split('/data/artifacts/txt')[1].rsplit('.', 1)[0]
    sentences = sent_tokenize(source_text)
    clumps = [sentences[i * 5:(i + 1) * 5] for i in range(len(sentences) // 5)]
    clumps.append(sentences[(len(sentences) // 5) * 5:len(sentences)])
    return [{'id': doc_id + '/' + str(i), 'content': c, 'sentence_count': len(c), 'fragment_nr': i} for i, c in
            enumerate(clumps)]


def setup_solr_index():
    print("Setting up Solr index:")
    for i, p in enumerate(txt_paths):
        with open(p, 'r') as in_f:
            raw = in_f.read()
        if not raw:
            continue
        data = get_solr_formatted_data(p, raw)
        solr.add(data)
        print(i)


if __name__ == '__main__':
    cur_dir = os.path.dirname(__file__)
    in_dir = os.path.join(cur_dir, '../data/source/')
    tei_dir = os.path.join(cur_dir, '../data/artifacts/tei/')
    txt_dir = os.path.join(cur_dir, '../data/artifacts/txt/')

    config_path = os.path.join(cur_dir, 'grobid/config.json')

    solr = pysolr.Solr(config['solr_base_uri'])
    extraction.grobid_extract(config_path, in_dir, tei_dir)
    tei_paths = get_all_paths(tei_dir, 'xml')
    extraction.tei_to_filtered_txt(tei_paths, 'txt')
    tei_paths = get_all_paths(tei_dir, 'xml')
    txt_paths = get_all_paths(txt_dir, 'txt')
    setup_solr_index()
    build_phrases(txt_paths, 2, sentences=SentenceIterator(txt_paths, format_word, word_ok,
                                                           line_split_concat))
    phrase_models = []
    for i in range(1, initialization_config['phrasing_passes'] + 1):
        phrase_models.append(
            Phraser.load(str(os.path.join(cur_dir, '../data/artifacts/embedding/phrases_model' + str(i)))))
    train_w2v_model(txt_paths, phrase_models, SentenceIterator(txt_paths, format_word, word_ok,
                                                               line_split_concat))
