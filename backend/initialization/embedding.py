import os

from gensim.models import Word2Vec
from gensim.models.phrases import Phrases, Phraser
from nltk.corpus import stopwords

from config import initialization_config
from initialization.w2v_sentence_iterator import SentenceIterator

cur_dir = os.path.dirname(__file__)
stop_words = set(stopwords.words(initialization_config['stop_words_language']))
w2v_out_path = os.path.join(cur_dir, '../data/artifacts/embedding/ris-w2v-corpus-trained')


def line_split_concat(word_list):
    out = []
    cur = ''
    for i in range(len(word_list)):
        if cur.endswith('-'):
            cur += word_list[i]
        else:
            if i > 0:
                out.append(cur)
            cur = word_list[i]
    out.append(cur)
    # if len(out) < len(word_list):
    #     print('CHANGE', set(word_list).symmetric_difference(set(out)))
    return out


def format_word(word):
    return word.lower()


def word_ok(word):
    p_word = format_word(word)
    if p_word not in stop_words \
            and any(c.isalpha() for c in p_word) \
            and 1 < len(p_word) < 50:
        return True
    return False


def build_phrases(paths, iterations=2, sentences=None):
    if not sentences:
        sentences = SentenceIterator(paths, line_split_concat)
    phrases = Phrases(sentences, min_count=10, threshold=20, max_vocab_size=100000000, delimiter=b'_')
    Phraser(phrases).save(str(os.path.join(cur_dir, '../data/artifacts/embedding/phrases_model1')))
    sentences = phrases[sentences]
    for i in range(iterations - 1):
        print('@ iteration ' + str(i))
        phrases = Phrases(sentences, min_count=5, threshold=10, max_vocab_size=100000000, delimiter=b'_')
        Phraser(phrases).save(str(os.path.join(cur_dir, '../data/artifacts/embedding/phrases_model' + str(i + 2))))
        sentences = phrases[sentences]
    return phrases


def train_w2v_model(paths, phrases_models=(), sentences=None):
    if not sentences:
        sentences = SentenceIterator(paths, line_split_concat)
    for model in phrases_models:
        sentences = model[sentences]
    model = Word2Vec(sentences, size=300)
    model.save(w2v_out_path)
