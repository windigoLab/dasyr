from pathlib import PurePosixPath, Path

from initialization.grobid.grobid_client import grobid_client
import xml.etree.ElementTree as ET
import os

N = 5
SERVICE = "processFulltextDocument"
GENERATE_IDS = False
CONSOLIDATE_HEADER = False
CONSOLIDATE_CITATIONS = False
INCLUDE_RAW_CITATIONS = False
INCLUDE_RAW_AFFILIATIONS = False
FORCE = False
TEI_COORDINATES = False
NSMAP = {'tei': 'http://www.tei-c.org/ns/1.0'}


def grobid_extract(config_path, input_path, output_path):
    client = grobid_client(config_path)
    client.process(input_path, output_path, N, SERVICE, GENERATE_IDS, CONSOLIDATE_HEADER, CONSOLIDATE_CITATIONS,
                   INCLUDE_RAW_CITATIONS, INCLUDE_RAW_AFFILIATIONS, FORCE, TEI_COORDINATES)


def tei_to_filtered_txt(tei_paths, txt_dir_name):
    for in_path in tei_paths:
        paragraphs = []
        tree = ET.parse(in_path)
        root = tree.getroot()
        for abstract in root.findall('.//tei:abstract/tei:p', namespaces=NSMAP):
            paragraphs.append(abstract.text)
        for p in root.findall('.//tei:body//tei:p', namespaces=NSMAP):
            paragraphs.append(p.text)
        out_path = in_path.replace('artifacts/tei/', 'artifacts/%s/' % txt_dir_name).replace('.tei.xml', '.txt')
        if not os.path.exists(Path(out_path).parents[0]):
            os.makedirs(Path(out_path).parents[0])
        with open(out_path, 'w+') as out_file:
            out_file.write('\n'.join(paragraphs))


if __name__ == '__main__':
    tei_to_filtered_txt(
        ['/home/fabian/projects/dasyr/backend/data/artifacts/tei/CLEF/CLEF-2013/CLEF2013wn-CHiC-TomsEt2013.tei.xml'],
        'txt')
