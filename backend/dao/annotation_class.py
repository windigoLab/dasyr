from db.database import db


class AnnotationClass(db.Model):
    name = db.Column(db.String(255), primary_key=True)
    description = db.Column(db.String(255), unique=False, nullable=True)
    annotations = db.relationship('Annotation', backref='annotation_class', lazy=True)

    def __repr__(self):
        return '<AnnotationClass %r>' % self.name
