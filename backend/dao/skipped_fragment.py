from db.database import db


class SkippedFragment(db.Model):
    id = db.Column(db.String(500), primary_key=True)
    username = db.Column(db.String(255), db.ForeignKey('user.username'))

    def __repr__(self):
        return '<SkippedFragment %r>' % self.id
