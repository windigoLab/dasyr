from db.database import db


class User(db.Model):
    username = db.Column(db.String(255), primary_key=True)
    password = db.Column(db.String(255), unique=False, nullable=False)
    annotations = db.relationship('Annotation', backref='user', lazy=True)

    def __repr__(self):
        return '<User %r>' % self.username
