from db.database import db


class Annotation(db.Model):
    __table_args__ = (db.UniqueConstraint('term_name', 'username', 'fragment_id', 'fragment_occurrence'),)
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    term_name = db.Column(db.String(255), db.ForeignKey('term.name'))
    username = db.Column(db.String(255), db.ForeignKey('user.username'))
    annotation_class_name = db.Column(db.String(255), db.ForeignKey('annotation_class.name'), nullable=False)
    fragment_id = db.Column(db.String(255), nullable=True)
    fragment_occurrence = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)
    in_context_annotation = db.Column(db.Boolean, nullable=False)
    seed = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return '<Annotation %r by %r: %r>' % (self.term_name, self.username, self.annotation_class_name)
