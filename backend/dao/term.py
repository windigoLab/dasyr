from db.database import db


class Term(db.Model):
    name = db.Column(db.String(255), primary_key=True)
    parent_id = db.Column(db.String(255), db.ForeignKey('term.name'), nullable=True)
    score = db.Column(db.Float(), unique=False, nullable=True)
    children = db.relationship('Term', backref='parent', remote_side=name, lazy=True,
                               cascade="all")
    annotations = db.relationship('Annotation', backref='term', lazy=True)

    def __repr__(self):
        return '<Term %r>' % self.name
