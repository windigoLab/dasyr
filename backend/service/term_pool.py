import re

import pysolr
from nltk.tokenize import word_tokenize

from config import config
from dao.annotation import Annotation
from dao.skipped_fragment import SkippedFragment
from dao.term import Term
from db.database import db
from controllers.annotation import format_word
from service.term_recommendation_engine import TermRecommendationEngine
from sqlalchemy import func, text
from sqlalchemy.orm import aliased


class TermPool:

    def __init__(self, n_last_garbage=10, n_most_similar=10):
        self.recommendation_engine = TermRecommendationEngine()
        self.solr = pysolr.Solr(config['solr_base_uri'])
        self.n_last_garbage = n_last_garbage
        self.n_most_similar = n_most_similar
        self.PRE = '<'
        self.POST = '>'

    @staticmethod
    def remove_annotation(term, username):
        Annotation.query.filter_by(username=username, term_name=term).delete()
        db.session.commit()

    @staticmethod
    def __get_next_terms(username):
        annotationA = aliased(Annotation)
        annotationB = aliased(Annotation)
        terms = Term.query.with_entities(Term.name, Term.score,func.count(annotationA.id).label('annotation_amount'), func.max(annotationB.annotation_class_name == 'None').label('none_max')) \
            .outerjoin(annotationA, (annotationA.username == username) & (annotationA.term_name == Term.name)) \
            .join(annotationB, Term.parent_id==annotationB.term_name) \
            .filter((annotationA.annotation_class_name != 'Garbage') | (annotationA.annotation_class_name == None)) \
            .group_by(Term.name).order_by(text('annotation_amount ASC'), text('none_max ASC'), Term.score.desc()).all()
        print(terms)
        terms = [Term(name=TermPool.__format_phrase(t[0]), score=t[1]) for t in terms]
        return terms

    @staticmethod
    def __get_current_garbage(fragment_content, username):
        if not fragment_content:
            return None
        words = {format_word(word) for sentence in fragment_content for word in word_tokenize(sentence)}
        garbage_terms = {a.term_name for a in
                         Annotation.query.filter_by(username=username, annotation_class_name='Garbage')}
        return list(garbage_terms.intersection(words))

    @staticmethod
    def __get_context_terms(terms, content, main_term):
        # print(content)
        sentence_string = '.'.join(
            [' %s ' % ' '.join([format_word(word) for word in word_tokenize(sentence)]) for sentence in content])
        # print(sentence_string)
        term_set = {t.name for t in terms if ' %s ' % t.name in sentence_string}
        term_set.discard(main_term.name)
        phrase_part_set = {part for phr in term_set for part in phr.split(' ') if ' ' in phr}
        # print(term_set)
        # print(phrase_part_set)
        return list(term_set - phrase_part_set)

    @classmethod
    def __format_phrase(cls, term):
        return term.replace('_', ' ')

    @staticmethod
    def __get_fragment_exclusion_clause(annotations, username):
        skipped_fragment_ids = [sf.id for sf in SkippedFragment.query.filter_by(username=username).all()]
        print(skipped_fragment_ids)
        if (annotations is None or len(annotations) == 0) and (
                skipped_fragment_ids is None or len(skipped_fragment_ids) == 0):
            return ''
        clause = ['-id: "%s"' % id for id in [a.fragment_id for a in annotations] + skipped_fragment_ids]
        # print(clause)
        return clause

    @staticmethod
    def __get_highlight_content(results, result):
        return results.highlighting[result['id']]['content']

    @staticmethod
    def __okay_fragment(content, phrase):
        pattern = r'\s'.join([r'<%s>' % part for part in phrase.split(' ')])
        pattern = pattern.replace('-', '>-<')
        res = bool(re.search(pattern, ' '.join(content), re.IGNORECASE))
        #if not res:
	    #print('BAD_CONTENT: ', content)
        return res

    def __get_term_fragment(self, username, terms):
        hl_dict = {
            'hl': 'true',
            'hl.preserveMulti': 'true',
            'hl.fragsize': 0,
            'hl.fl': 'content',
            'hl.simple.pre': self.PRE,
            'hl.simple.post': self.POST,
            'hl.requireFieldMatch': 'true',
            'hl.usePhraseHighlighter': 'true',
        }
        for term in terms:
            annotations = Annotation.query.filter_by(term_name=term.name.replace(' ', '_')).filter(
                (Annotation.fragment_id != None) & (Annotation.username == username)).all()
            clause = self.__get_fragment_exclusion_clause(annotations, username)
            results = self.solr.search(q='content:"%s"' % term.name, fq=clause, **hl_dict)
            # print(term)
            # print(len(results))
            for result in results:
                # print(result)
                highlight_content = self.__get_highlight_content(results, result)
                if self.__okay_fragment(highlight_content, term.name):
                    return term, result
            print("term '%s' discarded, due to no fitting fragments" % term)
        raise Exception("Out of valid terms/fragments!")

    def get_last_garbage(self, username):
        garbage_annotations = Annotation.query.with_entities(Annotation.term_name).filter_by(
            annotation_class_name='Garbage', username=username).order_by(Annotation.created_at.desc()).limit(
            self.n_last_garbage).distinct()
        # garbage_annotations = Annotation.query.filter_by(username=username, annotation_class_name='Garbage') \
        #     .order_by(Annotation.created_at.desc()).limit(self.n_last_garbage).all()
        return [a.term_name for a in garbage_annotations]

    def next(self, username):
        terms = self.__get_next_terms(username)
        last_garbage = self.get_last_garbage(username)
        term, fragment = self.__get_term_fragment(username, terms)
        current_garbage = self.__get_current_garbage(fragment.get('content', None), username)
        context_terms = self.__get_context_terms(terms, fragment.get('content'), term)
        res = {
            'term': term.name,
            'fragmentContent': fragment.get('content', None),
            'fragmentId': fragment.get('id', None),
            'lastGarbage': last_garbage,
            'currentGarbage': current_garbage,
            'contextTerms': context_terms
        }
        print(res)
        return res

    def submit(self, annotation):
        if annotation.annotation_class_name != 'Garbage':
            all_terms = [term.name for term in Term.query.all()]
            most_sim_terms = self.recommendation_engine.get_n_most_similar(annotation.term_name, 10)
            for term in most_sim_terms:
                if term[0] not in all_terms:
                    db.session.add(Term(name=term[0], score=term[1], parent_id=annotation.term_name))
        db.session.add(annotation)
        db.session.commit()
