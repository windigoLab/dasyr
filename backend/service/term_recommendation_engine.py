import os

import gensim

from config import config


class TermRecommendationEngine:

    def __init__(self):
        self.embedding = self.initialize_embedding()

    @staticmethod
    def initialize_embedding():
        cur_dir = os.path.dirname(__file__)
        in_path = os.path.join(cur_dir, '../data/artifacts/embedding/' + config['w2v_file_name'])

        return gensim.models.Word2Vec.load(in_path)

    def get_n_most_similar(self, term, n):
        try:
            res = self.embedding.most_similar([term], topn=n)
        except Exception as e:
            print(e)
            res = []
        # print(res)
        return res
