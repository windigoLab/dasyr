from flask import Blueprint, request, jsonify
from flask_jwt_extended import create_access_token
from passlib.hash import sha256_crypt

from dao.user import User

auth = Blueprint('auth', __name__, url_prefix='/api')


@auth.route('/auth', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Use JSON in request"}), 400

    username = request.get_json().get('username', None)
    password = request.get_json().get('password', None)

    if not (username and password):
        return jsonify({"msg": "Provide username and password in JSON request"}), 400

    user = User.query.filter_by(username=username).first()

    if not user or not sha256_crypt.verify(password, user.password):
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token)
