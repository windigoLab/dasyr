from datetime import datetime

import flask
from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from dao.annotation import Annotation
from dao.annotation_class import AnnotationClass
from dao.skipped_fragment import SkippedFragment
from dao.term import Term
from db.database import db
from initialization.embedding import format_word
from service.term_pool import TermPool

annotation = Blueprint('annotation', __name__, url_prefix='/api')

term_pool = TermPool()


@annotation.route('/annotation/seed', methods=['POST'])
@jwt_required
def create_seeds():
    if Annotation.query.all():
        return flask.Response(status=400)
    data = request.get_json()
    seed_annotations = []
    for seed_class in data:
        annotation_class = AnnotationClass(name=seed_class['name'])
        db.session.add(annotation_class)
        for seed_term in seed_class['terms']:
            term = Term(name=format_word(seed_term))
            seed_annotations.append(Annotation(term_name=term.name, username=get_jwt_identity(),
                                               annotation_class_name=annotation_class.name,
                                               created_at=datetime.now(), in_context_annotation=False, seed=True))

            db.session.add(term)
    
    db.session.commit()
    for seed_annotation in seed_annotations:
        term_pool.submit(seed_annotation)
    db.session.commit()
    Annotation.query.all()
    return flask.Response(status=201)


@annotation.route('/annotation/seed', methods=['GET'])
@jwt_required
def get_seeds():
    seeds = []
    annotation_classes = AnnotationClass.query.all()
    annotations = Annotation.query.filter_by(seed=True).all()
    for annClass in annotation_classes:
        seed = {'name': annClass.name,
                'terms': [a.term_name for a in annotations if a.annotation_class_name == annClass.name]}
        seeds.append(seed)
    return jsonify(seeds)


@annotation.route('/annotation/class', methods=['GET'])
@jwt_required
def get_annotation_classes():
    annotation_classes = AnnotationClass.query.all()
    return jsonify([{'name': ac.name, 'description': ac.description} for ac in annotation_classes])


@annotation.route('/annotation/garbage', methods=['GET'])
@jwt_required
def get_last_garbage():
    return jsonify(term_pool.get_last_garbage(get_jwt_identity()))


@annotation.route('/annotation/garbage/<string:term>', methods=['DELETE'])
@jwt_required
def ungarbage(term):
    term_pool.remove_annotation(term, get_jwt_identity())
    return flask.Response(status=200)


@annotation.route('/annotation', methods=['POST'])
@jwt_required
def submit_annotations():
    annotations = request.get_json()
    print(annotations)
    for data in annotations:
        new_annotation = Annotation(username=get_jwt_identity(),
                                    term_name=format_word(data['term'].replace(' ', '_')),
                                    created_at=datetime.now(),
                                    fragment_id=data.get('fragmentId'),
                                    fragment_occurrence=data.get('occurrence'),
                                    in_context_annotation=data.get('isContextAnnotation'),
                                    annotation_class_name=data['annotationClass'],
                                    seed=False)
        term_pool.submit(new_annotation)
    return flask.Response(status=200)


@annotation.route('/annotation/next', methods=['GET'])
@jwt_required
def get_next_term_context():
    res = term_pool.next(get_jwt_identity())
    if res:
        return jsonify(res)
    else:
        return flask.Response(status=404)


@annotation.route('/annotation/skip', methods=['POST'])
@jwt_required
def skip_fragment():
    data = request.get_json()
    skipped_fragment = SkippedFragment(id=data['id'], username=get_jwt_identity())
    db.session.add(skipped_fragment)
    db.session.commit()
    return flask.Response(status=200)
