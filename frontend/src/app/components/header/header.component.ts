import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {MatDialog} from '@angular/material/dialog';
import {LogoutDialogComponent} from './logout-dialog/logout-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService, public dialog: MatDialog) {
  }

  username: string;

  ngOnInit(): void {
    this.setUsername();
    this.authService.getAuthChange().subscribe((change) => this.setUsername());
  }

  setUsername(): void {
    if (this.authService.isAuthenticated()) {
      this.username = this.authService.getIdentity();
    } else {
      this.username = undefined;
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(LogoutDialogComponent, {
      width: '250px',
      data: {name: this.username}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
