import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER, SPACE} from '@angular/cdk/keycodes';
import {Seed} from '../../dtos/seed';
import {AnnotationService} from '../../services/annotation.service';

@Component({
  selector: 'app-seeds',
  templateUrl: './seeds.component.html',
  styleUrls: ['./seeds.component.css']
})
export class SeedsComponent implements OnInit {

  selectable = false;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  seeds: Seed[] = [
    {name: 'Garbage', terms: []},
    {name: 'None', terms: []},
  ];
  alreadyCreated = false;

  constructor(private authService: AuthService, private router: Router, private annotationService: AnnotationService) {
  }

  getNonDefaultSeeds() {
    return this.seeds.filter((seed) => ['None', 'Garbage'].indexOf(seed.name) < 0);
  }

  ngOnInit(): void {
    this.annotationService.loadSeeds().subscribe(
      seeds => {
        if (seeds.length > 0) {
          this.seeds = seeds;
          this.alreadyCreated = true;
        }
      },
      error => alert(error.message)
    );
    console.log(this.seeds);
  }


  addClass(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    const classNames = this.seeds.map((seed) => seed.name);
    // Add our fruit
    if ((value || '').trim() && !classNames.includes(value.trim())) {
      this.seeds.push({name: value.trim(), terms: []});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeClass(seed: Seed): void {
    const index = this.seeds.indexOf(seed);

    if (index >= 0) {
      this.seeds.splice(index, 1);
    }
  }

  removeTerm(seed: Seed, term: string) {
    const index = this.seeds.indexOf(seed);

    if (index >= 0) {
      const termIndex = this.seeds[index].terms.indexOf(term);

      if (termIndex >= 0) {
        this.seeds[index].terms.splice(termIndex, 1);
      }

    }
  }

  addTerm(event: MatChipInputEvent, seed: Seed) {
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      const index = this.seeds.indexOf(seed);
      const allTerms = this.seeds.reduce((acc, cur) => acc.concat(cur.terms), []);
      if (!allTerms.includes(value.trim())) {
        this.seeds[index].terms.push(value.trim());
      }
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  onSaveSeeds() {
    console.log(this.seeds);
    this.annotationService.submitSeeds(this.seeds).subscribe(
      () => this.router.navigate(['annotation']),
      (error) => alert(error.message)
    );
  }

  handleFileInput(event: any) {
    console.log(event);
    if (event.target.files.length > 0) {
      const file = event.target.files.item(0);
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
        const res = JSON.parse(fileReader.result as string);
        const newSeeds = [];
        for (const category in res) {
          if (res.hasOwnProperty(category)) {
            newSeeds.push({name: category, terms: res[category]});
          }
        }
        if (!('None' in res)) {
          newSeeds.unshift({name: 'None', terms: []});
        }
        if (!('Garbage' in res)) {
          newSeeds.unshift({name: 'Garbage', terms: []});
        }
        this.seeds = newSeeds
      };
      fileReader.readAsText(file);
    } else {
      console.log('No file given.');
    }

  }
}
