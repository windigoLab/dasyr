import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {AnnotationService} from '../../../services/annotation.service';

@Component({
  selector: 'app-context-annotation-dialog',
  templateUrl: './context-annotation-dialog.component.html',
  styleUrls: ['./context-annotation-dialog.component.css']
})

export class ContextAnnotationDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ContextAnnotationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private annotationService: AnnotationService,
    ) {
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }

  ngOnInit(): void {
  }

  onClassSelect(className: string) {
    this.dialogRef.close(className);
  }
}
