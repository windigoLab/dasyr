import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextAnnotationDialogComponent } from './context-annotation-dialog.component';

describe('ContextAnnotationDialogComponent', () => {
  let component: ContextAnnotationDialogComponent;
  let fixture: ComponentFixture<ContextAnnotationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextAnnotationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextAnnotationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
