import {Component, OnInit} from '@angular/core';
import {AnnotationClass} from '../../dtos/annotation-class';
import {AuthService} from '../../auth/auth.service';
import {AnnotationService} from '../../services/annotation.service';
import {AnnotationContext} from '../../dtos/annotation-context';
import {UngarbageDialogComponent} from './ungarbage-dialog/ungarbage-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {Annotation} from '../../dtos/annotation';
import {ContextAnnotationDialogComponent} from './context-annotation-dialog/context-annotation-dialog.component';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-annotation',
  templateUrl: './annotation.component.html',
  styleUrls: ['./annotation.component.css']
})
export class AnnotationComponent implements OnInit {
  annotationClasses: AnnotationClass[];
  annotationContext: AnnotationContext;
  seedsCreated = false;
  loading = true;
  splitFragment;
  occurrenceNumbers: any;
  mainTermOccurrences: number;
  annotationMap: {};
  currentMainOccurrence: number;
  panelOpenState = false;
  adminMail = environment.adminMail;

  constructor(private authService: AuthService, private annotationService: AnnotationService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.annotationService.loadSeeds().subscribe(
      seeds => {
        seeds.length > 0 ? this.seedsCreated = true : this.seedsCreated = false;
        if (this.seedsCreated) {
          this.loadAnnotationClasses();
          this.next();
        }
      }
    );
  }

  loadAnnotationClasses(): void {
    this.annotationService.getAnnotationClasses().subscribe(
      (classes) => {
        this.annotationClasses = classes;
      }
    );
  }

  initializeAnnotations(): void {
    this.currentMainOccurrence = 1;
    this.annotationMap = {};
    this.annotationMap[this.annotationContext.term] = {};
    for (const term of this.annotationContext.contextTerms) {
      this.annotationMap[term] = {};
    }
  }

  getAnnotationsFromMap(): Annotation[] {
    const annotations = [];
    Object.entries(this.annotationMap).forEach(termEntry => Object.entries(termEntry[1]).forEach(
      occurrenceEntry => annotations.push(new Annotation(termEntry[0], occurrenceEntry[1], this.annotationContext.fragmentId,
        Number(occurrenceEntry[0]), termEntry[0] !== this.annotationContext.term))
    ));

    return annotations;
  }

  addAnnotation(term: string, occurrence: number, annotationClass: string): void {
    this.annotationMap[term][occurrence] = annotationClass;
    if (term === this.annotationContext.term) {
      this.currentMainOccurrence++;
      if (this.currentMainOccurrence >= this.mainTermOccurrences || annotationClass === 'Garbage') {
        this.loading = true;
        this.submitAnnotations();
      }
    }
  }

  submitAnnotations(): void {
    this.annotationService.submitAnnotations(this.getAnnotationsFromMap()).subscribe(
      () => this.next(),
      error => alert(error)
    );
  }

  next(): void {
    this.loading = true;
    this.annotationService.getNextAnnotationContext().subscribe(
      (annCont) => {
        this.annotationContext = annCont;
        console.log(this.annotationContext);
        this.setSplitFragment();
        console.log(this.splitFragment);
        this.initializeAnnotations();
        this.loading = false;
      },
      (error) => {
        alert(error.message);
        this.loading = false;
      }
    );
  }

  onUngarbage(termName): void {
    const dialogRef = this.dialog.open(UngarbageDialogComponent, {
      width: '400px',
      data: {term: termName}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      const currentIndex = this.annotationContext.currentGarbage ? this.annotationContext.currentGarbage.indexOf(termName) : -1;
      const lastIndex = this.annotationContext.lastGarbage.indexOf(termName);
      if (currentIndex >= 0) {
        this.annotationContext.currentGarbage.splice(currentIndex, 1);
      }
      if (lastIndex >= 0) {
        this.annotationContext.lastGarbage.splice(lastIndex, 1);
        this.annotationService.getLastGarbage().subscribe(values => this.annotationContext.lastGarbage = values);
      }
    });
  }

  getSplitRegex(): RegExp {
    const termSet = new Set();

    termSet.add(this.annotationContext.term);
    this.annotationContext.contextTerms.forEach(val => termSet.add(val));
    this.annotationContext.currentGarbage.forEach(val => termSet.add(val));
    this.annotationContext.lastGarbage.forEach(val => termSet.add(val));
    const mainPattern = Array.from(termSet).join('|');
    return new RegExp(String.raw`(^|\W)?(?<!\w)(${mainPattern})($|\W)`, 'i');
  }

  setOccurrenceNumbers() {
    this.occurrenceNumbers = [];
    let n = 1;
    const contextOccurrences = {};
    for (const contextTerm of this.annotationContext.contextTerms) {
      contextOccurrences[contextTerm] = 1;
    }
    for (const part of this.splitFragment) {
      const fPart = this.formatWord(part);
      if (fPart === this.annotationContext.term) {
        this.occurrenceNumbers.push(n);
        n++;
      } else if (contextOccurrences.hasOwnProperty(fPart)) {
        this.occurrenceNumbers.push(contextOccurrences[fPart]);
        contextOccurrences[fPart] += 1;
      } else {
        this.occurrenceNumbers.push(-1);
      }
    }
    this.mainTermOccurrences = n;
  }

  formatWord(word: string) {
    return word.toLowerCase();
  }

  setSplitFragment() {
    const joinedFragmentContent = this.annotationContext.fragmentContent.join(' ');
    this.splitFragment = joinedFragmentContent.split(this.getSplitRegex()).filter(v => v !== undefined);
    this.setOccurrenceNumbers();
  }

  wordIsGarbage(word) {
    return this.annotationContext.currentGarbage.indexOf(this.formatWord(word)) > -1
      || this.annotationContext.lastGarbage.indexOf(this.formatWord(word)) > -1;
  }

  wordIsActiveContextTerm(part: string, occurrenceNumber: number) {
    const fTerm = this.formatWord(part);
    return this.annotationContext.contextTerms.indexOf(fTerm) > -1
      && (!this.annotationMap[fTerm].hasOwnProperty(occurrenceNumber));
  }

  onContextTermClick(part: string, occurrenceNumber: number) {
    console.log(part, occurrenceNumber);
    const dialogRef = this.dialog.open(ContextAnnotationDialogComponent, {
      width: '400px',
      data: {term: this.formatWord(part), annotationClasses: this.annotationClasses}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result != null) {
        this.addAnnotation(this.formatWord(part), occurrenceNumber, result);
      }
    });
  }

  skipFragment(fragmentId: string) {
    this.annotationService.skipFragment({id: fragmentId}).subscribe(
      () => this.next(),
      (error) => alert(error)
    );
  }
}
