import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {AnnotationService} from '../../../services/annotation.service';

@Component({
  selector: 'app-ungarbage-dialog',
  templateUrl: './ungarbage-dialog.component.html',
  styleUrls: ['./ungarbage-dialog.component.css']
})
export class UngarbageDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UngarbageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private annotationService: AnnotationService,
    private router: Router) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  onUngarbageClick() {
    this.annotationService.ungarbage(this.data.term).subscribe(() => console.log('success'));
    this.dialogRef.close(true);
  }
}
