import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UngarbageDialogComponent} from './ungarbage-dialog.component';

describe('UngarbageDialogComponent', () => {
  let component: UngarbageDialogComponent;
  let fixture: ComponentFixture<UngarbageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UngarbageDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UngarbageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
