import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../dtos/user';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) {
  }

  user: User;
  showSpinner: boolean;

  ngOnInit() {
    this.showSpinner = false;
    this.user = new User();
  }

  login(): void {
    this.authService.login(this.user).subscribe(
      data => this.handleLoginSuccess(data.access_token),
      error => alert(error)
    );
  }

  handleLoginSuccess(accessToken): void {
    localStorage.setItem('jwt_token', accessToken);
    this.authService.submitAuthChange(true);
    this.router.navigate(['/']);
  }
}
