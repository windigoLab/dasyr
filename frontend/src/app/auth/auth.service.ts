import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../dtos/user';
import {environment} from '../../environments/environment';
import {Observable, Subject} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authBaseUri = environment.backendUri + '/auth';

  private authChange = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  getAuthChange(): Observable<boolean> {
    return this.authChange.asObservable();
  }

  submitAuthChange(dir): void {
    this.authChange.next(dir);
  }

  login(user: User): Observable<any> {
    return this.http.post<User>(this.authBaseUri, user);
  }

  logout(): void {
    localStorage.removeItem('jwt_token');
    this.submitAuthChange(false);
  }

  getToken(): string {
    return localStorage.getItem('jwt_token');
  }

  isAuthenticated(): boolean {
    const token = this.getToken();
    return token && !helper.isTokenExpired(token);
  }

  getIdentity(): string {
    const token = this.getToken();
    return helper.decodeToken(token).identity;
  }
}
