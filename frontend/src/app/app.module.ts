import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {SeedsComponent} from './components/seeds/seeds.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from './components/header/header.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import {HttpClientModule} from '@angular/common/http';
import {JwtModule} from '@auth0/angular-jwt';
import {MatIconModule} from '@angular/material/icon';
import {LogoutDialogComponent} from './components/header/logout-dialog/logout-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
import {AnnotationComponent} from './components/annotation/annotation.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {UngarbageDialogComponent} from './components/annotation/ungarbage-dialog/ungarbage-dialog.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatBadgeModule} from '@angular/material/badge';
import { ContextAnnotationDialogComponent } from './components/annotation/context-annotation-dialog/context-annotation-dialog.component';
import {MatExpansionModule} from '@angular/material/expansion';

export function jwtTokenGetter() {
  return localStorage.getItem('jwt_token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SeedsComponent,
    HeaderComponent,
    LogoutDialogComponent,
    AnnotationComponent,
    UngarbageDialogComponent,
    ContextAnnotationDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: jwtTokenGetter,
        allowedDomains: ['localhost:5000'],
      }
    }),
    MatIconModule,
    MatDialogModule,
    MatChipsModule,
    MatGridListModule,
    MatDividerModule,
    MatBadgeModule,
    MatExpansionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
