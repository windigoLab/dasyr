export class Annotation {
  public term: string;
  public annotationClass: string;
  public fragmentId: string;
  public occurrence: number;
  public isContextAnnotation: boolean;

  constructor(term: string, className: string, fragmentId: string, occurrence: number, isContextAnnotation: boolean) {
    this.term = term;
    this.annotationClass = className;
    this.fragmentId = fragmentId;
    this.occurrence = occurrence;
    this.isContextAnnotation = isContextAnnotation;
  }
}
