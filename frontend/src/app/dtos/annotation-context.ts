export class AnnotationContext {
  public term: string;
  public fragmentContent: string[];
  public fragmentId: string;
  public currentGarbage: string[];
  public lastGarbage: string[];
  public contextTerms: string[];

}
