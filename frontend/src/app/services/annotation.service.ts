import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AnnotationClass} from '../dtos/annotation-class';
import {Observable} from 'rxjs';
import {Seed} from '../dtos/seed';
import {AnnotationContext} from '../dtos/annotation-context';
import {Annotation} from '../dtos/annotation';

@Injectable({
  providedIn: 'root'
})
export class AnnotationService {

  annotationBaseUri = environment.backendUri + '/annotation';

  constructor(private http: HttpClient) {
  }

  getAnnotationClasses(): Observable<AnnotationClass[]> {
    return this.http.get<AnnotationClass[]>(this.annotationBaseUri + '/class');
  }

  submitSeeds(seeds: Seed[]): Observable<any> {
    return this.http.post(this.annotationBaseUri + '/seed', seeds);
  }

  getNextAnnotationContext(): Observable<AnnotationContext> {
    return this.http.get<AnnotationContext>(this.annotationBaseUri + '/next');
  }

  submitAnnotation(annotation: Annotation): Observable<any> {
    return this.http.post(this.annotationBaseUri, annotation);
  }

  loadSeeds(): Observable<Seed[]> {
    return this.http.get<Seed[]>(this.annotationBaseUri + '/seed');
  }

  getLastGarbage(): Observable<string[]> {
    return this.http.get<string[]>(this.annotationBaseUri + '/garbage');
  }

  ungarbage(termName: string) {
    return this.http.delete(this.annotationBaseUri + '/garbage/' + termName);
  }

  submitAnnotations(annotations: Annotation[]): Observable<any> {
    return this.http.post(this.annotationBaseUri, annotations);
  }

  skipFragment(fragmentId: { id: string }): Observable<any> {
    return this.http.post(this.annotationBaseUri + '/skip', fragmentId);
  }
}
