import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {SeedsComponent} from './components/seeds/seeds.component';
import {AnnotationComponent} from './components/annotation/annotation.component';
import {AuthGuardService} from './auth/auth-guard.service';


const routes: Routes = [
  {path: '', component: SeedsComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent},
  {path: 'annotation', component: AnnotationComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
